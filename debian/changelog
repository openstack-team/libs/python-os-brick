python-os-brick (6.11.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 26 Feb 2025 13:55:29 +0100

python-os-brick (6.9.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090548).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 09:32:52 +0100

python-os-brick (6.9.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 17:13:18 +0200

python-os-brick (6.9.0-1) experimental; urgency=medium

  * New upstream release.
  * Add new (build-)depends: python3-psutil.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 11:46:22 +0200

python-os-brick (6.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Apr 2024 09:12:50 +0200

python-os-brick (6.7.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Feb 2024 17:15:53 +0100

python-os-brick (6.4.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * d/watch: use version 4 and gitmode.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Oct 2023 13:54:44 +0200

python-os-brick (6.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed version in python3-pbr build-depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Aug 2023 09:38:40 +0200

python-os-brick (6.2.2-3) unstable; urgency=medium

  * Cleans better (Closes: #1048337).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Aug 2023 09:05:11 +0200

python-os-brick (6.2.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 10:59:34 +0200

python-os-brick (6.2.2-1) experimental; urgency=medium

  * New upstream point release:
    - Fixes CVE-2023-2088: Unauthorized volume access through deleted volume
      attachments. (Closes: #1035932).

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 May 2023 13:34:21 +0200

python-os-brick (6.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed versions of (build-)depends when satisfied in Bookworm.
  * Removed python3-eventlet from depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 09:13:24 +0100

python-os-brick (6.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 11:40:32 +0200

python-os-brick (6.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-oslo.config as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Aug 2022 09:10:16 +0200

python-os-brick (5.2.2-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Aug 2022 13:13:36 +0200

python-os-brick (5.2.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 14:04:53 +0100

python-os-brick (5.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Feb 2022 17:30:24 +0100

python-os-brick (5.0.1-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Oct 2021 11:21:00 +0200

python-os-brick (5.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 10:55:40 +0200

python-os-brick (5.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Aug 2021 10:19:30 +0200

python-os-brick (4.3.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 14:50:13 +0200

python-os-brick (4.3.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Mar 2021 11:57:15 +0200

python-os-brick (4.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 10:51:56 +0100

python-os-brick (4.0.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 13:26:05 +0200

python-os-brick (4.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 16:45:37 +0200

python-os-brick (4.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Sep 2020 16:00:37 +0200

python-os-brick (3.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Added python3-tenacity to (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Jun 2020 19:19:01 +0200

python-os-brick (3.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 18:11:15 +0200

python-os-brick (3.0.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Apr 2020 22:22:00 +0200

python-os-brick (2.10.2-1) unstable; urgency=medium

  * New upstream point release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 26 Mar 2020 12:32:59 +0100

python-os-brick (2.10.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 09:47:00 +0200

python-os-brick (2.10.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Rebased removes-privacy-breach-in-docs.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Sep 2019 09:21:14 +0200

python-os-brick (2.8.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Jul 2019 22:03:57 +0200

python-os-brick (2.8.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Tue, 26 Mar 2019 17:03:48 +0100

python-os-brick (2.5.5-1) unstable; urgency=medium

  * New upstream point release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Feb 2019 14:31:55 +0100

python-os-brick (2.5.4-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Jan 2019 23:10:27 +0100

python-os-brick (2.5.3-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Sep 2018 23:19:21 +0200

python-os-brick (2.5.3-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Add trailing tilde to min version depend to allow
    backports
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using Python 3 for building sphinx doc.
  * Rebased removes-privacy-breach-in-docs.patch.
  * Added a few <!nocheck> in debian/control.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 16:57:59 +0200

python-os-brick (2.3.0-3) unstable; urgency=medium

  * Blacklist 2 timeout tests which don't seem deterministic.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Feb 2018 23:41:52 +0000

python-os-brick (2.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 22:56:33 +0000

python-os-brick (2.3.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.
  * Use github tag over HTTPS for watch file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Feb 2018 08:50:24 +0000

python-os-brick (1.15.3-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 01:13:13 +0000

python-os-brick (1.15.3-1) experimental; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Oct 2017 23:21:45 +0200

python-os-brick (1.2.0-4) unstable; urgency=medium

  * Really add open-iscsi to solve live migration issues.

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Jun 2016 13:44:41 +0000

python-os-brick (1.2.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * Add open-iscsi to solve live migration issues (LP: #1588799).

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Jun 2016 15:40:05 +0200

python-os-brick (1.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 20:09:12 +0000

python-os-brick (1.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed version of python{3,}-oslo.concurrency for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 09:19:50 +0200

python-os-brick (1.1.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).

  [ Corey Bryant ]
  * New upstream release.
  * d/control: Align (Build-)Depends with upstream.
  * d/p/drop-reno-sphinxext.patch: Drop reno-sphinxext from sphinx config.

  [ Thomas Goirand ]
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 18:06:03 +0000

python-os-brick (0.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Testing with all Python versions.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Mon, 18 Jan 2016 12:22:45 +0000

python-os-brick (0.5.0-2) unstable; urgency=medium

  [ James Page ]
  * d/rules: Drop use of -S flag with dpkg-parsechangelog to ease backports.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 10:11:23 +0000

python-os-brick (0.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Added a new os-brick-common package, which is needed to hold the rootwrap
    configuration file for both python{3,}-os-brick packages.
  * Added -doc privacy breanch fix.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2015 16:07:58 +0200

python-os-brick (0.3.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 14:26:34 +0000

python-os-brick (0.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #789601).

 -- James Page <james.page@ubuntu.com>  Mon, 22 Jun 2015 14:29:41 +0100

Source: python-os-brick
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 James Page <james.page@ubuntu.com>,
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-babel,
 python3-castellan,
 python3-coverage,
 python3-ddt,
 python3-eventlet,
 python3-hacking,
 python3-openstackdocstheme <!nodoc>,
 python3-os-win,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.privsep,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.utils,
 python3-oslo.vmware,
 python3-oslotest <!nocheck>,
 python3-psutil,
 python3-reno,
 python3-requests,
 python3-retrying,
 python3-stestr <!nocheck>,
 python3-tenacity,
 python3-testscenarios <!nocheck>,
 python3-testtools <!nocheck>,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-os-brick
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-os-brick.git
Homepage: https://github.com/openstack/os-brick

Package: os-brick-common
Architecture: all
Depends:
 ${misc:Depends},
Description: Library for managing local volume attaches - common files
 OpenStack Cinder brick library for managing local volume attaches.
 .
 Features discovery of volumes being attached to a host for many
 transport protocols and removal of volumes from a host.
 .
 This package contains common files needed by either the Python 2 or 3 module
 of os-brick.

Package: python-os-brick-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Library for managing local volume attaches - doc
 OpenStack Cinder brick library for managing local volume attaches.
 .
 Features discovery of volumes being attached to a host for many
 transport protocols and removal of volumes from a host.
 .
 This package contains the documentation.

Package: python3-os-brick
Architecture: all
Depends:
 open-iscsi,
 os-brick-common (= ${binary:Version}),
 python3-babel,
 python3-os-win,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.privsep,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.utils,
 python3-pbr,
 python3-psutil,
 python3-requests,
 python3-retrying,
 python3-tenacity,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-os-brick-doc,
Description: Library for managing local volume attaches - Python 3.x
 OpenStack Cinder brick library for managing local volume attaches.
 .
 Features discovery of volumes being attached to a host for many
 transport protocols and removal of volumes from a host.
 .
 This package contains the Python 3.x module.
